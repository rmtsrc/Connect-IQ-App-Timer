using Toybox.Application as App;
using Toybox.Timer as Timer;
using Toybox.WatchUi as Ui;
using Toybox.Attention as Attention;
using Toybox.ActivityRecording as ActivityRecording;
using Toybox.Sensor as Sensor;
using Toybox.Time as Time;

class TimerModel{

	var counter;
	var status=:Wait;
	var data;
	var viewVisible=true;
		
	function initialize(data){
		self.data=data;
	}

	function startTimer() {
		counter = data.time;
		var cron = Cron.getInstance();
		cron.unregister(data.id);
		cron.register(data.id, 1000, method(:refresh), true);
		resumeTimer();
	}

	function resumeTimer() {
		Cron.getInstance().enable(data.id);
		status=:Work;
		startBuzz();
		Ui.requestUpdate();
	}

	function pauseTimer() {
		Cron.getInstance().disable(data.id);
		status=:Wait;
		stopBuzz();
		Ui.requestUpdate();
	}
	
	function stopTimer(beep){
		if (status == :Work) {
			status=:Stop;
			if (beep) {
				stopBuzz();
			}
		}
		Cron.getInstance().unregister(data.id);
		Ui.requestUpdate();
	}
	
	function resetTimer() {
		status=:Reset;
		Cron.getInstance().unregister(data.id);
		Ui.requestUpdate();
	}
		
	function setStatus(status){
		self.status=status;
	}	
	
	function refresh(){
		status=:Work;
		if (counter > 1) {
			counter--;
			if (counter <= 5) {
				buzz();
				if(!viewVisible){
					Ui.pushView(new TimerView(data), new TimerDelegate(data), Ui.SLIDE_IMMEDIATE );
				}
			}
		} else {
			status=:Stop;
			end();	
		}
		Ui.requestUpdate();
	}

	function end(){
		stopTimer(false);
		endBuzz();
		if(data.autoRestart){
			startTimer();
		}
	}
			
	function startBuzz() {
		if(data.beep && Attention has :playTone){
			beep(Attention.TONE_START);
		}
	}

	function stopBuzz() {
		if(data.beep && Attention has :playTone){
			beep(Attention.TONE_STOP);
		}
	}

	function endBuzz() {
		if(data.beep && Attention has :playTone){
			beep(Attention.TONE_SUCCESS);
		}
		vibrate(1000);
	}
	
	function buzz() {
		if(data.beep && data.beep5s && Attention has :playTone){
			beep(Attention.TONE_LOUD_BEEP);
		}
	}
	
	function vibrate(duration) {
		if(Attention has :vibrate && data.vibrate){
			var vibrateData = [
				new Attention.VibeProfile(100, duration),
				new Attention.VibeProfile(0, 500),
				new Attention.VibeProfile(100, duration),
				new Attention.VibeProfile(0, 500),
				new Attention.VibeProfile(100, duration)
			];
			Attention.vibrate(vibrateData);
		}
	}

	function beep(tone) {
		if( Attention has :playTone ){
			Attention.playTone(tone);
		}
	}

	function setVisible(visible){
		self.viewVisible=visible;
	}
}
