using Toybox.Application as App;
using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;

class TouchTimePickerView extends Ui.View {

	hidden var data;

	hidden var start;
	hidden var smallFont;
	hidden var text_width_small;
	hidden var text_height_small;

	function initialize(data) {
		View.initialize();
		self.data=data;

		start = Ui.loadResource(Rez.Strings.Start);
		smallFont = Ui.loadResource(Rez.Fonts.LargeFont);
	}

	function getTimerText() {
		var timerText = self.data.time.toString();
		var length = timerText.length();

		if (length == 0) {
			return "";
		}

		if (length <= 2) {
			return timerText + ":00";
		}

		if (length >= 5) {
			var zerosToAdd = 6 - length;
			for (var idx = 0; idx < zerosToAdd; idx++) {
				timerText = timerText + "0";
			}

			var numbers = timerText.toCharArray();
			var hours = (numbers[0] + numbers[1]).toNumber();
			var mins = (numbers[2] + numbers[3]).toNumber();
			var secs = (numbers[4] + numbers[5]).toNumber();

			return hours + ":" + mins + ":" + secs;
		}

		var zerosToAdd = 4 - length;
		for (var idx = 0; idx < zerosToAdd; idx++) {
			timerText = timerText + "0";
		}

		var numbers = timerText.toCharArray();
		var mins = (numbers[0] + numbers[1]).toNumber();
		var secs = (numbers[2] + numbers[3]).toNumber();

		return mins + ":" + secs;
	}

	function onUpdate(dc) {
		dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_BLACK);
		dc.clear();

		var timerText = getTimerText();

		if (timerText.length() != 0) {
			dc.drawText(125, 64, Gfx.FONT_SMALL, "<", Gfx.TEXT_JUSTIFY_LEFT);
			dc.drawText(dc.getWidth()/2, 125, Gfx.FONT_NUMBER_MILD, timerText, Gfx.TEXT_JUSTIFY_VCENTER|Gfx.TEXT_JUSTIFY_CENTER);
			dc.drawText(108, 160, Gfx.FONT_SMALL, start, Gfx.TEXT_JUSTIFY_LEFT);
		}

		dc.drawText(140, 30, Gfx.FONT_NUMBER_MEDIUM, "0", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(197, 47, Gfx.FONT_NUMBER_MEDIUM, "1", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(238, 93, Gfx.FONT_NUMBER_MEDIUM, "2", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(238, 160, Gfx.FONT_NUMBER_MEDIUM, "3", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(200, 210, Gfx.FONT_NUMBER_MEDIUM, "4", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(140, 228, Gfx.FONT_NUMBER_MEDIUM, "5", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(83, 210, Gfx.FONT_NUMBER_MEDIUM, "6", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(45, 160, Gfx.FONT_NUMBER_MEDIUM, "7", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(45, 93, Gfx.FONT_NUMBER_MEDIUM, "8", Gfx.TEXT_JUSTIFY_VCENTER);
		dc.drawText(80, 47, Gfx.FONT_NUMBER_MEDIUM, "9", Gfx.TEXT_JUSTIFY_VCENTER);
	}

}