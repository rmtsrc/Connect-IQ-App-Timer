using Toybox.Application as App;
using Toybox.WatchUi as Ui;

var elements= new [0];
var models =  {};
class TimerApp extends App.AppBase {

    function initialize() {
        AppBase.initialize();
    }

    function onStart(state) {
    	extract(App.getApp().getProperty("elements"));
    	//extract("[{\"name\":\"A\",\"time\":\"15\",\"beep\":\"true\",\"beep5s\":\"true\",\"vibrate\":\"true\"},{\"name\":\"B\",\"time\":\"60\",\"beep\":\"true\",\"beep5s\":\"true\",\"vibrate\":\"true\"},{\"name\":\"C\",\"time\":\"60\",\"beep\":\"true\",\"beep5s\":\"true\",\"vibrate\":\"true\"},{\"name\":\"D\",\"time\":\"60\",\"beep\":\"true\",\"beep5s\":\"true\",\"vibrate\":\"true\"}]");
    }

    function onStop(state) {
    	App.getApp().setProperty("elements",elementsToJson());
    }
    
    function getInitialView() {
    	var menu = new HomeMenu ();
			return [ menu, new HomeMenuDelegate(menu) ];
    }
	
	
	function extract(json){
		if(json!=null){
			var index =json.find("{");
			var position = 0;
			while(index != null){
				json = json.substring(index +1 ,json.length());
				var value = json.substring(0,json.find("}")+1);			
				elements.add(new Element(position,"{"+value));
				index =json.find("{");
				position = position + 1;
			}
		}
	}
		
	function elementsToJson(){
		var json = "";
		var elSize = elements.size();
		var start = (elSize > 10) ? elSize - 10 : 0;
		for (var i=start; i < elSize; i++){
			if(i==0){
				json= "[";
			}
			json = json+elements[i].toJson();
			if(i<elSize-1){
				json = json+",";
			}else{
				json = json+"]";
			}
		}
		return json;
	}
	
}
