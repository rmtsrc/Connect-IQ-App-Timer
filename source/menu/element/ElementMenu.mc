using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class ElementMenu extends Menu {

	hidden var data;
	hidden var start;
	hidden var reset;
	hidden var autoRestart;
	hidden var continu;
	hidden var alarms;
	hidden var remove;
	hidden var rename;
	hidden var up;
	hidden var down;
	
	function initialize (data) {
		self.data=data;
		continu = Ui.loadResource(Rez.Strings.Continu);
		start = Ui.loadResource(Rez.Strings.Start);
		reset = Ui.loadResource(Rez.Strings.Reset);
		autoRestart = Ui.loadResource(Rez.Strings.AutoRestart);
		alarms = Ui.loadResource(Rez.Strings.Alarms);
		remove = Ui.loadResource(Rez.Strings.Remove);
		rename = Ui.loadResource(Rez.Strings.Rename);
		up = Ui.loadResource(Rez.Strings.Up);
		down = Ui.loadResource(Rez.Strings.Down);
		
		var menuItems = new [0];
		menuItems.add(new DataMenuItem (:Start,start, data));
		menuItems.addAll(defaultMenu());
    	    
		Menu.initialize (menuItems, data.id);
	}
	
	function defaultMenu(){
		var menuItems = new [0];
		menuItems.add(new TimeMenuItem (:Timer,data));
		menuItems.add(new StateMenuItem (:AutoRestart,autoRestart,data,"autoRestart"));
		menuItems.add(new DataMenuItem (:Alarms,alarms,data));
		if (Ui has :TextPicker) {
			menuItems.add(new DataMenuItem (:Rename,rename,data));
		}
		menuItems.add(new DataMenuItem (:Up,up,data));
		menuItems.add(new DataMenuItem (:Down,down,data));
		menuItems.add(new DataMenuItem (:Remove,remove,data));
		return menuItems;
	}
	
	function onShow() {
		if(models.hasKey(data.id) && models.get(data.id).status==:Work){
			var menuItems = new [0];
			menuItems.add(new DataMenuItem (:Continu,continu, data));
			menuItems.add(new DataMenuItem (:Reset,reset, data));
    	menuItems.addAll(defaultMenu());

			Menu.setMenuArray(menuItems);
		}       		
    
		Menu.setTitle(data.name);
		Menu.onShow();
	}
	
	function onUpdate(dc) {	
		if(models.hasKey(data.id) && models.get(data.id).status==:Reset){
			var menuItems = new [0];
			menuItems.add(new DataMenuItem (:Start,start, data));
			menuItems.addAll(defaultMenu());

			Menu.setMenuArray(menuItems);
			Menu.resetPosition();
			models.get(data.id).setStatus(:Wait);
		}
		Menu.onUpdate(dc);
	}	
}