using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class DataMenuItem extends MenuItem{
	var data;
	
	function initialize (id, label, data) {
		self.data=data;
		MenuItem.initialize(id, label);
	}
	
}