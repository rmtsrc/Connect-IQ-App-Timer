using Toybox.WatchUi as Ui;
using Toybox.Math;
using Toybox.System;

class HomeMenuDelegate extends MenuDelegate {

	hidden var timerStr;
	hidden var currItem;

	function initialize(menu) {
		timerStr = Ui.loadResource(Rez.Strings.Timer);

		MenuDelegate.initialize(menu);
	}

	function onMenuItem(item) {
		if (item.id == :Add) {
			var newTimerId = elements.size() + 1;
			var element = new Element(newTimerId, "{\"id\":\""+Math.rand()+"\",\"name\":\""+timerStr+"\"}");
			elements.add(element);
			var menu = new ElementMenu(element);

			var deviceSettings = System.getDeviceSettings();
			if (
				deviceSettings.isTouchScreen &&
				deviceSettings.screenShape == System.SCREEN_SHAPE_ROUND && 
				deviceSettings.screenWidth == 260 && 
				deviceSettings.screenHeight == 260)
			{
				Ui.pushView(new TouchTimePickerView(element), new TouchTimePickerDelegate(element), Ui.SLIDE_IMMEDIATE);
			} else {
				Ui.pushView(new TimePicker(timerStr, element), new TimePickerDelegate(element), Ui.SLIDE_IMMEDIATE);
			}
		} else {
			var model;
			if (models.hasKey(item.data.id)) {
				model = models.get(item.data.id);
			} else {
				model = new TimerModel(item.data);
				models.put(item.data.id, model);
			}

			if (model.status != :Work) {
				model.startTimer();
			}

			Ui.pushView(new TimerView(item.data), new TimerDelegate(item.data), Ui.SLIDE_IMMEDIATE);
		}
	}

	function onKey(keyEvent) {
		if (keyEvent.getKey() == 4) {
			var item = menu.selectedItem();
			if (item.id == :Add) {
				return true;
			}


			var timer;
			if (models.hasKey(item.data.id)) {
				timer = models.get(item.data.id);
			} else {
				timer = new TimerModel(item.data);
				models.put(item.data.id, timer);
			}

			if (timer.status == :Work) {
				timer.resetTimer();
			} else {
				timer.startTimer();
			}
		}
		return true;
	}

	function onHold(clickEvent) {
		var item = menu.selectedItem();
		if (item.id != :Add) {
			var elMenu = new ElementMenu(item.data);
			Ui.pushView(elMenu, new ElementMenuDelegate(elMenu), Ui.SLIDE_LEFT);		
		}
	}

	function onMenu() {
		var item = menu.selectedItem();
		if (item.id != :Add) {
			var elMenu = new ElementMenu(item.data);
			Ui.pushView(elMenu, new ElementMenuDelegate(elMenu), Ui.SLIDE_LEFT);		
		}
	}
	
	function onBack() {
		var values = models.values();
		for(var i=0; i<values.size(); i++) {
			values[i].resetTimer();
		}
	}
}