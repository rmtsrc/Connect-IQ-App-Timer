using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class ElementMenuItem extends DataMenuItem {

	hidden var timerStr;

	function initialize (id, data) {
		timerStr = Ui.loadResource(Rez.Strings.Timer);

		DataMenuItem.initialize(id, data.id, data);
	}
		
	function onUpdate (dc) {
		if(models.hasKey(data.id) && models.get(data.id).status == :Work){
			DataMenuItem.setLabel(Utils.timeToString(models.get(data.id).counter));
		}else{
			DataMenuItem.setLabel(Utils.timeToString(data.time));
		}

		if (data.name.equals(timerStr)) {
			DataMenuItem.setFont(Gfx.FONT_NUMBER_HOT);
		} else {
			DataMenuItem.setValue(data.name);
		}
	}
}