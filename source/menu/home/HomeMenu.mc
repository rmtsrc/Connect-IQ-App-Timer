using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class HomeMenu extends Menu {

	hidden var add;

	function initialize () {
		Menu.initialize(null, null);
		add = Ui.loadResource(Rez.Strings.Add);
	}
	
	function onShow() {
		var menuArray = new [elements.size()+1];
		
		for (var i=0; i< elements.size(); i++) {
			menuArray[i] = new ElementMenuItem(i, elements[i]);
		}
		
		menuArray[elements.size()] = new MenuItem(:Add, add);
				
		Menu.setMenuArray(menuArray);
		Menu.onShow();
	}	
}