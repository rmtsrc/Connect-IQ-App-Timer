using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class TimeMenuItem extends DataMenuItem{
	function initialize (id, data) {
		DataMenuItem.initialize(id,  Utils.timeToString(data.time),data);
		DataMenuItem.setFont(Gfx.FONT_NUMBER_HOT);
	}
	
	function onShow () {
		DataMenuItem.setLabel(Utils.timeToString(data.time)); 
	}
}