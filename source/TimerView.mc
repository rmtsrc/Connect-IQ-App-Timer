using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as System;

class TimerView extends Ui.View {

	hidden var text_width_10;
	hidden var text_width_1;
	hidden var text_width_point;
	hidden var text_width_small;
	hidden var text_height_large;
	hidden var text_height_small;
	hidden var cx;
	hidden var cy;
	hidden var data;
	hidden var largeFont;
	hidden var smallFont;
	hidden var complete;
	hidden var paused;
	
	function initialize(data) {
		View.initialize();
		self.data=data;
		largeFont =  Ui.loadResource(Rez.Fonts.LargeFont);
		smallFont =  Ui.loadResource(Rez.Fonts.SmallFont);
		complete = Ui.loadResource(Rez.Strings.Complete);
		paused = Ui.loadResource(Rez.Strings.Paused);
	}

	function onShow () {
	}
		
	function onLayout(dc) {
		cx = dc.getWidth() / 2;
		cy = dc.getHeight() / 2;
		text_width_10 = dc.getTextWidthInPixels("88",largeFont);
		text_width_1 = dc.getTextWidthInPixels("8",largeFont);
		text_height_large = dc.getFontHeight(largeFont);
		text_width_point = dc.getTextWidthInPixels(":",largeFont);
		text_width_small=dc.getTextWidthInPixels("88",smallFont);
		text_height_small = dc.getFontHeight(smallFont);
	}

	function onUpdate (dc) {
		dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_BLACK);
		dc.clear();
		var timer = models.get(data.id);
		if(timer.status == :Work || timer.status == :Wait) {
			drawTime(timer.counter, dc);
			bottomText(data.name,dc);
			drawArc(dc, timer.counter, data.time);

			if (timer.status == :Work) {
				var clock = System.getClockTime();
				topText(clock.hour.format("%02d") + ":" + clock.min.format("%02d"), dc);
			} else {
				topText(paused, dc);
			}
		} else {
			dc.drawText(dc.getWidth()/2, dc.getHeight()/2, Gfx.FONT_LARGE, data.name + " " + complete, Gfx.TEXT_JUSTIFY_VCENTER|Gfx.TEXT_JUSTIFY_CENTER);
			bottomText(Utils.timeToString(data.time), dc);
		}
	}

	function drawTime(long, dc) {
		var seconds = long % 60;
		var minutes = (long / 60) % 60;
		var hours = long / 60 /60;
		var start_minute;
		var start_point;

		if(hours>0){
			var start_hour;
			if(hours>=10){
				start_hour=(dc.getWidth()-(text_width_10+text_width_point+text_width_10+text_width_small+8))/2;
				start_point = start_hour+text_width_10+2;
			}else{
				start_hour=(dc.getWidth()-(text_width_1+text_width_point+text_width_10+text_width_small+8))/2;
				start_point = start_hour+text_width_1+2;
			}
			dc.drawText(start_hour, cy, largeFont, hours, Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_LEFT);
			start_minute = start_point+text_width_point+2;
			minutes = minutes.format("%02d");
		}else if (minutes>=10) {
			start_minute=(dc.getWidth()-(text_width_10+text_width_point+text_width_10+4))/2;
			start_point=start_minute+text_width_10+2;
		}else{
			start_minute=(dc.getWidth()-(text_width_1+text_width_point+text_width_10+4))/2;
			start_point=start_minute+text_width_1+2;
		}
		
		dc.drawText(start_minute, cy, largeFont, minutes, Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_LEFT);
		dc.drawText(start_point, cy, largeFont, ":", Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_LEFT);
		
		if(hours>0){
			var start_seconds=start_point+text_width_point+text_width_10+4;
			dc.drawText(start_seconds, cy-text_height_large/2+text_height_small/2, smallFont, seconds.format("%02d"), Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_LEFT);
		}else{
			var start_seconds=start_point+text_width_point+2;
			dc.drawText(start_seconds, cy, largeFont, seconds.format("%02d"), Gfx.TEXT_JUSTIFY_VCENTER | Gfx.TEXT_JUSTIFY_LEFT);
		}
	}
	
	function drawArc(dc,counter,max){
		var cx =  dc.getWidth()/2;
		var cy = dc.getHeight()/2;
		dc.setPenWidth(6);
		dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
			
		var angle = 0;
		if(counter >0){
			angle= counter*360/max;
		}
		if(angle>0){
			dc.drawArc(cx,cy,dc.getHeight()/2-6,Gfx.ARC_CLOCKWISE,90,(360-angle.toLong()+90)%360); 
		}
	}
	
	function topText (text, dc) {
		dc.drawText(dc.getWidth()/2, dc.getHeight()*0.175, Gfx.FONT_MEDIUM, text, Gfx.TEXT_JUSTIFY_VCENTER |Gfx.TEXT_JUSTIFY_CENTER);
	}
		
	function bottomText (text, dc) {
		dc.drawText(dc.getWidth()/2, dc.getHeight()*0.825, Gfx.FONT_MEDIUM, text, Gfx.TEXT_JUSTIFY_VCENTER |Gfx.TEXT_JUSTIFY_CENTER);
	}
}
