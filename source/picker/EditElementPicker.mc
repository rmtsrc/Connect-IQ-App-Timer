using Toybox.WatchUi as Ui;

class EditElementPicker extends Ui.TextPickerDelegate {
	hidden var data;

    function initialize(data) {
    	self.data=data;
        Ui.TextPickerDelegate.initialize();
    }

    function onTextEntered(text, changed) {
    	data.name = text;
    }

    function onCancel() {
        //Nothing
    }
}