module Utils{

	function timeToString(long){
		var seconds = long % 60;
		var minutes = (long / 60) % 60;
		var hours =  long / 60 / 60;
		if(hours>0){
			return hours+":"+minutes.format("%02d")+":"+seconds.format("%02d");
		}else{
			return minutes+":"+seconds.format("%02d");
		}
	}
}