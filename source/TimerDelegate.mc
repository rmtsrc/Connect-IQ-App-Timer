using Toybox.WatchUi as Ui;

class TimerDelegate extends Ui.BehaviorDelegate {
	
	hidden var data;
	
	function initialize(data) {
		BehaviorDelegate.initialize();
		self.data=data;
		models.get(data.id).setVisible(true);
	}

	function onSelect() {
		var timer = models.get(data.id);
		if (timer.status != :Work) {
			timer.resetTimer();
			Ui.popView(Ui.SLIDE_RIGHT);
		}
	}

	function onKey(keyEvent) {
		if (keyEvent.getKey() == 4) {
			var timer = models.get(data.id);
			if (timer.status == :Work) {
				timer.pauseTimer();
			} else if (timer.status == :Wait) {
				timer.resumeTimer();
			} else {
				timer.resetTimer();
				timer.startTimer();
			}
		}
		return true;
	}

	function onHold(clickEvent) {
		Ui.popView(Ui.SLIDE_IMMEDIATE);
		var menu = new ElementMenu(data);
		Ui.pushView(menu, new ElementMenuDelegate(menu), Ui.SLIDE_LEFT);
		return true;
	}

	function onMenu() {
		Ui.popView(Ui.SLIDE_IMMEDIATE);
		var menu = new ElementMenu(data);
		Ui.pushView(menu, new ElementMenuDelegate(menu), Ui.SLIDE_LEFT);
	}

	function onBack() {
		models.get(data.id).setVisible(false);
	}
}
